package com.service;

import com.dto.Pageinfo;
import com.dto.Pagers;
import com.entity.Article;
import com.entity.User;
import com.vo.ArticleVo;
/**
 * 文章服务
 * ArticleService.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月13日 下午1:16:06 
 * @version
 * @user micrxdd
 */
public interface ArticleService {
    /**
     * 根据文章分类id获取文章列表
     * @param pageinfo 分页信息
     * @param id 分类id
     * @return 文章列表
     */
    public Pagers ArticleListById(Pageinfo pageinfo,Integer id);
    /**
     * 保存文章
     * @param articleVo
     * @param user 用户
     */
    public void SaveArticle(ArticleVo articleVo,User user);
    /**
     * 根据文章id返回文章
     * @param id
     * @return
     */
    public Article FindById(Integer id);
    /**
     * 根据文章id返回文章
     * @param id
     * @return
     */
    public ArticleVo FindVoById(Integer id);
    /**
     * 更新文章
     * @param articleVo
     * @param user
     */
    public void UpdateArticle(ArticleVo articleVo,User user);
    /**
     * 删除文章
     * @param ids
     */
    public void DelArticle(Integer[] ids);
}
