package com.dao;

import com.entity.Newsitem;
import com.entity.Newstype;

public interface NewsItemDao extends BaseDao<Newsitem>{

    void UpdateNewsType(Newstype oldnewstype, Newstype newnewsstype);
}
