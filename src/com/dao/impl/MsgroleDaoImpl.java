package com.dao.impl;

import org.springframework.stereotype.Repository;

import com.dao.MsgroleDao;
import com.entity.Msgrole;
@Repository("msgroleDao")
public class MsgroleDaoImpl extends BaseDaoImpl<Msgrole> implements MsgroleDao{

}
