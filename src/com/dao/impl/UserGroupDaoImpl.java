package com.dao.impl;

import org.springframework.stereotype.Repository;

import com.dao.UserGroupDao;
import com.entity.Group;
@Repository("userGroupDao")
public class UserGroupDaoImpl extends BaseDaoImpl<Group> implements UserGroupDao{

}
