package com.dao.impl;

import org.springframework.stereotype.Repository;

import com.dao.EventDao;
import com.entity.Event;
@Repository("eventDao")
public class EventDaoImpl extends BaseDaoImpl<Event> implements EventDao{

}
