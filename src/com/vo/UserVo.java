package com.vo;
/**
 * UserVo.java
 * @author  microxdd
 * @version 创建时间：2014 2014年9月14日 下午5:22:36 
 * micrxdd
 * 
 */
public class UserVo {
    private String username;
    private String password;
    private String va;
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getVa() {
        return va;
    }
    public void setVa(String va) {
        this.va = va;
    }
    
}
