package com.weixin.msg;

import javax.xml.bind.annotation.XmlElement;
public class Msg {
	
	protected String toUserName;
	protected String fromUserName;
	protected Long createTime;
	protected String msgType;
	public Msg() {
		// TODO Auto-generated constructor stub
		this.createTime=System.currentTimeMillis();
	}
	
	@XmlElement(name="ToUserName")
	public String getToUserName() {
		return toUserName;
	}
	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}
	@XmlElement(name="FromUserName")
	public String getFromUserName() {
		return fromUserName;
	}
	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}
	@XmlElement(name="CreateTime")
	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
	@XmlElement(name="MsgType")
	public String getMsgType() {
		return msgType;
	}
	
	
	
}
