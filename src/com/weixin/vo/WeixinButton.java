package com.weixin.vo;

import java.util.ArrayList;
import java.util.List;

import com.weixin.exception.WeixinMenuOutOfBoundException;

public class WeixinButton {

	// 一级菜单数组，个数应为1~3个
	public List<WeixinMenu> button = new ArrayList<WeixinMenu>(3);

	
	/**
	 * 添加一级菜单
	 * @param menu 
	 * @throws WeixinMenuOutOfBoundException
	 */
	public void addMenu(WeixinMenu menu) throws WeixinMenuOutOfBoundException {
		if(button.size() < 3){
			button.add(menu);
		}else{
			throw new WeixinMenuOutOfBoundException();
		}
	}
	
	
}
