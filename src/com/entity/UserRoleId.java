package com.entity;

/**
 * UserRoleId entity. @author MyEclipse Persistence Tools
 */

public class UserRoleId implements java.io.Serializable {

    // Fields

    private User user;
    private String menusids;

    // Constructors

    /** default constructor */
    public UserRoleId() {
    }

    /** full constructor */
    public UserRoleId(User user, String menusids) {
	this.user = user;
	this.menusids = menusids;
    }

    // Property accessors

    public User getUser() {
	return this.user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    public String getMenusids() {
	return this.menusids;
    }

    public void setMenusids(String menusids) {
	this.menusids = menusids;
    }

    public boolean equals(Object other) {
	if ((this == other))
	    return true;
	if ((other == null))
	    return false;
	if (!(other instanceof UserRoleId))
	    return false;
	UserRoleId castOther = (UserRoleId) other;

	return ((this.getUser() == castOther.getUser()) || (this.getUser() != null
		&& castOther.getUser() != null && this.getUser().equals(
		castOther.getUser())))
		&& ((this.getMenusids() == castOther.getMenusids()) || (this
			.getMenusids() != null
			&& castOther.getMenusids() != null && this
			.getMenusids().equals(castOther.getMenusids())));
    }

    public int hashCode() {
	int result = 17;

	result = 37 * result
		+ (getUser() == null ? 0 : this.getUser().hashCode());
	result = 37 * result
		+ (getMenusids() == null ? 0 : this.getMenusids().hashCode());
	return result;
    }

}