package com.entity;

/**
 * Config entity. @author MyEclipse Persistence Tools
 */

public class Config implements java.io.Serializable {

    // Fields

    private String strkey;
    private String strvalue;

    // Constructors

    /** default constructor */
    public Config() {
    }

    /** full constructor */
    public Config(String strvalue) {
	this.strvalue = strvalue;
    }

    // Property accessors

    public String getStrkey() {
	return this.strkey;
    }

    public void setStrkey(String strkey) {
	this.strkey = strkey;
    }

    public String getStrvalue() {
	return this.strvalue;
    }

    public void setStrvalue(String strvalue) {
	this.strvalue = strvalue;
    }

}