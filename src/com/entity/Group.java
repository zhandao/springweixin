package com.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * Group entity. @author MyEclipse Persistence Tools
 */

public class Group implements java.io.Serializable {

    // Fields

    private Integer id;
    private String groupname;
    private String groupdes;
    private String menusids;
    private Set users = new HashSet(0);

    // Constructors

    /** default constructor */
    public Group() {
    }

    /** minimal constructor */
    public Group(String groupname) {
	this.groupname = groupname;
    }

    /** full constructor */
    public Group(String groupname, String groupdes, String menusids, Set users) {
	this.groupname = groupname;
	this.groupdes = groupdes;
	this.menusids = menusids;
	this.users = users;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getGroupname() {
	return this.groupname;
    }

    public void setGroupname(String groupname) {
	this.groupname = groupname;
    }

    public String getGroupdes() {
	return this.groupdes;
    }

    public void setGroupdes(String groupdes) {
	this.groupdes = groupdes;
    }

    public String getMenusids() {
	return this.menusids;
    }

    public void setMenusids(String menusids) {
	this.menusids = menusids;
    }

    public Set getUsers() {
	return this.users;
    }

    public void setUsers(Set users) {
	this.users = users;
    }

}